import { useEffect, useState } from "react";

import FormControlLabel from "@mui/material/FormControlLabel";
import IconButton from "@mui/material/IconButton";
import TaskIcon from "@mui/icons-material/Task";
import CardGiftcardIcon from "@mui/icons-material/CardGiftcard";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

import { useParams } from "react-router-dom";

import { History } from "history";
import { useHistory } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../hooks";

import classes from "./index.module.scss";

import ContentLayout from "../../../layout/ContentLayout";

import { User as UserType } from "../../../types";
import { getOneChild } from "../../../store/children/actions";

import { runEmitter, runListener } from "../../../utils/websockets";
import { Socket } from "socket.io-client";

import ChildLocationDialog from "./components/ChildLocationDialog";

interface LocationData {
  longitude?: string;
  latitude?: string;
  street?: string;
  town?: string;
}

const SpecificChildPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const currentUser = useAppSelector((state) => state.user.loggedInUser);
  const selectedChild = useAppSelector((state) => state.children.selectedChild);
  const { id, groupId } = useParams<{ id: string; groupId: string }>();
  const history = useHistory<History>();

  const [image, setImage] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [points, setPoints] = useState<number>(0);
  const [user, setUser] = useState<UserType | undefined>();
  const [locationData, setLocationData] = useState<LocationData>({
    longitude: "",
    latitude: "",
    street: "",
    town: "",
  });
  const [isBackdrop, setIsBackdrop] = useState<boolean>(false);

  const [isLocationDialogOpen, setIsLocationDialogOpen] =
    useState<boolean>(false);

  useEffect(() => {
    setUser(currentUser);
  }, [currentUser]);

  useEffect(() => {
    if (id && user) {
      if (id === user?._id) {
        setImage(user.profilePhoto);
        setName(user.name);
        setPoints(((user?.pointsCount as any)[groupId] as number) ?? 0);
      } else {
        dispatch(getOneChild(groupId, id));
      }
    }
  }, [id, user, dispatch, groupId]);

  useEffect(() => {
    if (selectedChild && selectedChild?._id !== user?._id) {
      setImage(selectedChild.profilePhoto);
      setName(selectedChild.name);
      setPoints((selectedChild.points as number) ?? 0);
    }
  }, [selectedChild, user]);

  const handleGetLocation = () => {
    const userId = currentUser?._id;
    const childId = selectedChild?._id;
    console.log("lecimy", userId, childId);

    runListener((socket: Socket) =>
      socket.once("returnChildLocation", (data) => {
        setIsBackdrop(false);
        console.log(data);
        const { longitude, latitude, address, city } = data;
        if (address && city) {
          setLocationData({ street: address, town: city });
        } else if (longitude && latitude) {
          setLocationData({ longitude, latitude });
        } else return;

        setIsLocationDialogOpen(true);
      })
    );
    setIsBackdrop(true);
    runEmitter("getChildLocation", { userId, childId });
  };

  const handleCloseLocationDialog = () => {
    setLocationData({ longitude: "", latitude: "", street: "", town: "" });
    setIsLocationDialogOpen(false);
  };

  if (!user) return <></>;

  return (
    <ContentLayout>
      <div className={classes.container}>
        <div className={classes.data}>
          <div
            className={classes.data__image}
            style={{ backgroundImage: `url(${image})` }}
          />
          <p className={classes.data__name}>{name}</p>
          <p className={classes.data__points}>{points} pts</p>
        </div>
        <div className={classes.menu}>
          <FormControlLabel
            control={
              <IconButton
                onClick={() => {
                  history.push("tasks/");
                }}
              >
                <TaskIcon />
              </IconButton>
            }
            label="Tasks"
          />
          <FormControlLabel
            control={
              <IconButton onClick={() => history.push("rewards/")}>
                <CardGiftcardIcon />
              </IconButton>
            }
            label="Rewards"
          />
          {user.role === "parent" && (
            <FormControlLabel
              control={
                <IconButton onClick={handleGetLocation}>
                  <LocationOnIcon />
                </IconButton>
              }
              label="Location"
            />
          )}
        </div>
      </div>
      <ChildLocationDialog
        open={isLocationDialogOpen}
        data={locationData}
        onClose={handleCloseLocationDialog}
      />
      <Backdrop
        sx={{ color: "#01dd24", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isBackdrop}
        onClick={() => {}}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </ContentLayout>
  );
};

export default SpecificChildPage;
