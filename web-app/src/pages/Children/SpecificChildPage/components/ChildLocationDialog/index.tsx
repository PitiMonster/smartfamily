import Button from "@mui/material/Button";

import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import { DialogActions, DialogContent } from "@mui/material";

export interface ChildLocationDialogProps {
  open: boolean;
  onClose: () => void;
  data: {
    longitude?: string;
    latitude?: string;
    street?: string;
    town?: string;
  };
}

const ChildLocationDialog = (props: ChildLocationDialogProps) => {
  const { onClose, open, data } = props;

  const handleClose = () => {
    onClose();
  };

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Your child location</DialogTitle>
      <DialogContent>
        {data.street && data.town ? (
          <>
            <p>Town: {data.town}</p>
            <p>Street: {data.street}</p>
          </>
        ) : (
          <>
            <p>Latitude: {data.latitude}</p>
            <p>Longitude: {data.longitude}</p>
          </>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} autoFocus>
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ChildLocationDialog;
