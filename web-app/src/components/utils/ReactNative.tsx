import * as React from "react";
import { useAppDispatch } from "../../hooks";
import { setReactNative } from "../../store/utils/actions";
import { handleFCMToken } from "../../store/user/actions";

import { History } from "history";
import { useHistory } from "react-router-dom";
import { RNMessage } from "../../types";

const ReactNative = () => {
  const dispatch = useAppDispatch();
  const isBrowser = typeof window !== "undefined";
  const history = useHistory<History>();

  const handleReactNativeMessages = React.useCallback(
    (msg: RNMessage) => {
      console.log(msg);
      if (!msg.type) return;
      switch (msg.type) {
        case "goBack":
          history.goBack();
          break;
        case "saveFcmToken":
          handleFCMToken(msg.data.token, "add");
          localStorage.setItem("fcmToken", msg.data.token);
          break;
        default:
          break;
      }
    },
    [history]
  );

  React.useEffect(() => {
    if (window.isRNWebView) {
      const handleMessageFromRN: EventListener = (event) => {
        let msg = (event as any).data;
        console.log(msg);
        if (msg) {
          msg = JSON.parse(msg);
          handleReactNativeMessages(msg);
        }
      };

      document.addEventListener("message", handleMessageFromRN);
      window.addEventListener("message", handleMessageFromRN);

      return () => {
        document.removeEventListener("message", handleMessageFromRN);
        window.removeEventListener("message", handleMessageFromRN);
      };
    }
  }, [handleReactNativeMessages]);

  React.useEffect(() => {
    if (isBrowser && window.isRNWebView) {
      dispatch(setReactNative(true));
      // document.body.style.userSelect = "none";
      document.body.classList.add("noselect");
    } else {
      // document.body.style.userSelect = "initial";
      document.body.classList.remove("noselect");
    }
  }, [window.isRNWebView]);
  return <></>;
};

export default ReactNative;
