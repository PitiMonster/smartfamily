import { useState, useEffect } from "react";

import classes from "./index.module.scss";

const ImageBlock: React.FC<{
  url: string;
  isPicked: boolean;
  name: string;
  onClick: () => void;
}> = ({ name, url, isPicked, onClick }) => {
  return (
    <div
      title={name.split(" ").join("")}
      onClick={onClick}
      style={{
        backgroundImage: `url(${url})`,
      }}
      className={
        isPicked ? [classes.image, classes.picked].join(" ") : classes.image
      }
    />
  );
};

type ImageObject = {
  url: string;
  name: string;
};

const ImageGalleryPicker: React.FC<{
  images: ImageObject[];
  pickedImage: string;
  setPickedImage: React.Dispatch<React.SetStateAction<string>>;
}> = ({ images, pickedImage, setPickedImage }) => {
  const [imagesList, setImagesList] = useState<
    | React.DetailedHTMLProps<
        React.HTMLAttributes<HTMLDivElement>,
        HTMLDivElement
      >[]
    | undefined
  >([]);

  useEffect(() => {
    const newImagesList = images.map((image) => (
      <ImageBlock
        name={image.name}
        url={image.url}
        isPicked={pickedImage === image.url}
        onClick={() => setPickedImage(image.url)}
      />
    ));

    setImagesList(newImagesList);
  }, [images, pickedImage, setPickedImage]);

  return <div className={classes.container}>{imagesList}</div>;
};

export default ImageGalleryPicker;
