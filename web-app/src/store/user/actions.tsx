import api from "../../api/api";
import { AppDispatch } from "..";
import { userActions } from "./slice";
import { utilsActions } from "../utils/slice";

export const getCurrentUser = () => {
  return async (dispatch: AppDispatch) => {
    try {
      const response = await api.get("/users/me");
      const user = response.data.data;
      dispatch(userActions.setLoggedInUser({ user }));
    } catch (err: any) {
      console.error("GET GROUPS ERROR: ", err);
      dispatch(
        utilsActions.setAppError({
          msg: err?.response?.data?.message ?? "Server error",
        })
      );
    }
  };
};

export const handleFCMToken = async (
  token: string,
  actionType: "add" | "delete"
) => {
  try {
    console.log("sending token:", actionType, token);
    await api.patch(`/users/FCMTokens/${actionType}`, { token });
  } catch (err) {}
};
