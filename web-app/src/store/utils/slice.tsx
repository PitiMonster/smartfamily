import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface UtilsState {
  isBackdrop: boolean;
  error: string;
  status: "success" | "fail" | null;
  isReactNative: boolean;
}

const initialState: UtilsState = {
  isBackdrop: false,
  error: "",
  status: null,
  isReactNative: false,
};

const utilsSlice = createSlice({
  name: "utils",
  initialState,
  reducers: {
    updateBackdrop(
      state,
      action: PayloadAction<{
        isBackdrop: boolean;
      }>
    ) {
      const { isBackdrop } = action.payload;
      state.isBackdrop = isBackdrop;
    },
    setAppError(state, action: PayloadAction<{ msg: string }>) {
      const { msg } = action.payload;
      state.error = msg;
    },
    setRequestStatus(
      state,
      action: PayloadAction<{
        status: "success" | "fail" | null;
      }>
    ) {
      const { status } = action.payload;

      state.status = status;
    },
    setIsReactNative(state, action: PayloadAction<{ isReactNative: boolean }>) {
      const { isReactNative } = action.payload;
      console.log("ser react", isReactNative);
      state.isReactNative = isReactNative;
    },
  },
});

export const utilsActions = utilsSlice.actions;

export default utilsSlice;
