import axios from "axios";
import { toastError } from "../utils/toasts";

const api = axios.create({
  baseURL:
    typeof Cypress !== "undefined"
      ? "http://127.0.0.1:8080/api/v1"
      : "https://immense-temple-96851.herokuapp.com/api/v1",
});

api.interceptors.request.use((config) => {
  config.headers.authorization =
    "Bearer " + localStorage.getItem("token") ?? null;
  return config;
});

api.interceptors.response.use((response) => {
  console.log("interceptors");
  console.log(response);

  if (response.status === 401) {
    toastError("Your session has expired. Log in again.");
    window.location.href = "https://smartfamily.vercel.app/auth/signin";
    localStorage.clear();
    window.location.reload();
  }

  return response;
});

export default api;
