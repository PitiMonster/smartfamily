import { io, Socket } from "socket.io-client";
import { AppDispatch } from "../store";

import { runNotificationSocketListeners } from "../store/notifications/actions";

let dispatch: AppDispatch;
export let socket: Socket;
export const runSocket = () => {
  if (!socket)
    socket = io("https://immense-temple-96851.herokuapp.com", {
      reconnectionDelay: 1000,
      reconnection: true,
      transports: ["websocket"],
      agent: false,
      upgrade: false,
      rejectUnauthorized: false,
    });
};

export const setDispatch = (disptachObj: AppDispatch) => {
  dispatch = disptachObj;
};

export const runAppListeners = () => {
  runNotificationSocketListeners(socket, dispatch);
};

export const runAppEmitters = (userId: string) => {
  runEmitter("connect notifications", { userId });
};

export const runListener = (listener: Function) => {
  listener(socket, dispatch);
};

export const offListener = (listener: Function) => {
  listener(socket);
};

export const runEmitter = (eventName: string, data: Object) => {
  socket.emit(eventName, data);
};
