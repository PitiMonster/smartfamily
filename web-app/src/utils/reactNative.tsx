import { RNMessage } from "../types";

export const sendMessageToRN = (msg: RNMessage) => {
  if (window.isRNWebView) {
    try {
      (window as any).ReactNativeWebView.postMessage(JSON.stringify(msg));
    } catch (e) {
      console.log("sending messageerror", e);
    }
  }
};
