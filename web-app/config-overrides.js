const path = require("path");

module.exports = {
  paths: function (paths, env) {
    paths.appIndexJs = path.resolve(__dirname, "instrumented/index.tsx");
    paths.appSrc = path.resolve(__dirname, "instrumented");
    return paths;
  },
};
