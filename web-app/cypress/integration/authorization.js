import moment from "moment";

/* eslint-disable cypress/no-unnecessary-waiting */
describe("authorization", () => {
  it("user can create an account", () => {
    // go to signup page
    cy.visit("127.0.0.1:3000/");
    cy.findByRole("button", { name: /sign up/i }).click();

    // fill signup data and submit
    cy.findByLabelText("Email").type("test@test.com");
    cy.findByLabelText("Password").type("password");
    cy.findByLabelText("Confirm password").type("password");
    cy.findByRole("button", { name: /sign up/i }).click();

    // fill user data
    cy.findByLabelText("Name").type("testName");
    cy.findByLabelText("Surname").type("testSurname");
    cy.findByLabelText("Username").type("testUsername");

    cy.get(".MuiTextField-root").click();
    cy.findByTestId("ArrowDropDownIcon").click();
    cy.findByRole("button", { name: /1999/i }).click();
    cy.findByRole("button", { name: /\.* 4, 1999/i }).click();
    cy.findByRole("button", { name: /ok/i }).click();

    cy.get(".MuiTextField-root > .MuiInput-root > .MuiInput-input").then(
      (date) => {
        expect(date[0].defaultValue.split("/")[2]).to.equal("1999");
      }
    );

    cy.get(".MuiFormGroup-root > :nth-child(2)").click();
    cy.findByRole("button", { name: /save/i }).click();

    // omit selecting photo
    cy.findByRole("button", { name: /submit/i }).click();

    // select role and submit
    cy.findByRole("button", { name: /submit/i }).click();

    // login to created account
    cy.findByLabelText("Email").type("test@test.com");
    cy.findByLabelText("Password").type("password");
    cy.findByRole("button", { name: /sign in/i }).click();

    cy.findByText(/create group/i);

    // create new group
    cy.findByTitle(/creategroup/i).click();
    cy.findByTitle(/grupa3/i).click();
    cy.findByRole("textbox", { name: /group name/i }).type("new group");
    cy.findByRole("button", { name: /save/i }).click();

    // get in new group
    cy.findByTitle(/newgroup/i).click();

    // go to calendar and create event
    cy.findByTitle(/calendar/i).click();
    cy.findByTestId("AddIcon").click();
    cy.findByRole("textbox", { name: /event name/i }).type("test event");
    cy.findByRole("textbox", { name: /minimum height/i }).type("test desc");
    cy.findByRole("button", { name: /save/i }).click();
    cy.wait(200);

    // check correction of created item
    cy.findByText(/test event/i).click();
    cy.wait(1000);
    cy.findByRole("textbox", { name: /event name/i }).then((value) => {
      console.log(value[0]);
      expect(value[0].defaultValue).to.equal("test event");
    });
    cy.findByRole("textbox", { name: /minimum height/i }).then((value) => {
      console.log(value[0]);
      expect(value[0].defaultValue).to.equal("test desc");
    });
    // cy.findByRole("button", { name: /save/i }).click();
    cy.findByTitle(/custom-backdrop/i).click("bottomRight");

    cy.go("back");
    cy.findByTitle(/budgets/i).click();

    // create budget
    cy.wait(2000);
    cy.findByTestId("AddIcon").click();
    cy.findByLabelText("Budget name").type("test budget");
    cy.findByLabelText("Budget value").type(1234);
    cy.findByRole("checkbox", { name: /renew periodically/i }).click();
    cy.findByRole("textbox", { name: /amount/i }).type(5);
    cy.findByRole("button", { name: /period days/i }).click();
    cy.findByRole("option", { name: /months/i }).click();
    cy.findByRole("textbox", { name: /next renewal date/i }).then(($date) => {
      expect($date[0].defaultValue).to.equal(
        moment().add(5, "months").format("yyyy/MM/DD")
      );
    });
    cy.wait(500);
    cy.findByRole("button", { name: /save/i }).click();
    cy.wait(200);

    // go into created budget
    cy.findByTitle(/testbudget/i).click();

    // add expense
    cy.findByTestId("AddIcon").click();
    cy.findByRole("textbox", { name: /expense name/i }).type("test expense");
    cy.findByRole("textbox", { name: /expense value/i }).type(123.4);
    cy.findByRole("textbox", { name: /minimum height/i }).type("test desc");
    cy.findByRole("button", { name: /save/i }).click();

    // check if created successfully
    cy.findByText(/test expense/i);
    cy.findByText(/123\.40zł/i);
    cy.findByText(/10\.00%/i);

    // check if actual budget data in 'Modify budget modal' is correct
    cy.findByTestId("CreateIcon").click();
    cy.findByRole("textbox", { name: /budget name/i }).then(($value) => {
      expect($value[0].defaultValue).to.equal("test budget");
    });
    cy.findByRole("textbox", { name: /budget value/i }).then(($value) => {
      expect($value[0].defaultValue).to.equal("1234");
    });
    cy.findByRole("textbox", { name: /next renewal date/i }).then(($value) => {
      expect($value[0].defaultValue).to.equal(
        moment().add(5, "months").format("yyyy/MM/DD")
      );
    });
    cy.findByTitle(/custom-backdrop/i).click("bottomRight");

    // go to shopping page
    cy.go("back");
    cy.go("back");
    cy.findByTitle(/shoppinglist/i).click();

    // add shopping item
    cy.findByTestId("AddIcon").click();
    cy.findByRole("textbox", { name: /item name/i }).type("testname");
    cy.findByRole("textbox", { name: /count/i }).type(123);
    cy.findByRole("textbox", { name: /minimum height/i }).type("test desc");
    cy.findByRole("button", { name: /save/i }).click();
    cy.wait(1000);

    // // check if created item exists
    // cy.findByText(/testname/i);
    cy.get("#testname > .MuiListItemText-primary");

    // remove created shopping item
    cy.findByRole("button", { name: /delete/i }).click();
    cy.wait(200);

    // add shopping item once again
    cy.findByTestId("AddIcon").click();

    cy.findByRole("textbox", { name: /item name/i }).type("testname");
    cy.findByRole("textbox", { name: /count/i }).type(123);
    cy.findByRole("textbox", { name: /minimum height/i }).type("test desc");
    cy.findByRole("button", { name: /save/i }).click();
    cy.wait(500);

    // check if created item exists
    cy.get("#testname > .MuiListItemText-primary");

    // test if not checked
    cy.findByRole("checkbox", {
      name: /testname testNametestUsername/i,
    }).should("not.be.checked");

    // check item
    cy.findByRole("checkbox", {
      name: /testname testNametestUsername/i,
    }).click();
    cy.wait(200);

    // test if checked
    cy.findByRole("checkbox", {
      name: /testname testNametestUsername/i,
    }).should("be.checked");
    cy.wait(200);

    // uncheck item
    cy.findByRole("checkbox", {
      name: /testname testNametestUsername/i,
    }).click();
    cy.wait(200);

    // check if not checked
    cy.findByRole("checkbox", {
      name: /testname testNametestUsername/i,
    }).should("not.be.checked");

    // check if item info data is correctly provided
    cy.findByRole("button", { name: /info/i }).click();
    cy.findByRole("textbox", { name: /item name/i }).then(($value) => {
      expect($value[0].defaultValue).to.equal("testname");
    });
    cy.findByRole("textbox", { name: /count/i }).then(($value) => {
      expect($value[0].defaultValue).to.equal("123");
    });
    cy.findByRole("textbox", { name: /minimum height/i }).then(($value) => {
      expect($value[0].defaultValue).to.equal("test desc");
    });
    cy.wait(300);
    cy.findByTitle(/custom-backdrop/i).click("bottomRight");
  });
});
