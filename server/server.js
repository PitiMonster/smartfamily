"use strict";
const mongoose = require("mongoose");
const cloudinary = require("cloudinary");
const dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });

const app = require("./app");
const runSockets = require("./Websockets");
const runSchedulers = require("./utils/runSchedulers");

const serviceAccount = require("./fcm_config.json");
const admin = require("firebase-admin");

const DB =
  process.env.NODE_ENV === "test"
    ? process.env.TEST_DATABASE.replace(
        "<PASSWORD>",
        process.env.DATABASE_PASSWORD
      )
    : process.env.DATABASE.replace("<PASSWORD>", process.env.DATABASE_PASSWORD);

// GLOBAL UNCAUGHT ERROR HANLDERS

process.on("uncaughtException", (err) => {
  console.error("UNCAUGHT EXCEPTION");
  console.log(err.name, err.message, err.stack);
  process.exit(1);
});

process.on("unhandledRejection", (err) => {
  console.error("UNHANDLED ERROR");
  console.log(err);
  server.close(() => {
    process.exit(1);
  });
});

mongoose
  .connect(DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
    autoIndex: true,
  })
  .then(() => {
    console.log("DB connection successful");

    if (process.env.NODE_ENV === "test") {
      mongoose.connection.db.dropDatabase(
        console.log(`${mongoose.connection.db.databaseName} databse dropped`)
      );
    }
  })
  .catch((err) => {
    console.error(err);
    console.log("DB CONNECTION ERROR");
  });

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

// Constants
const port = process.env.PORT || 8080;

const server = app.listen(port, async () => {
  await runSchedulers();
  console.log(`App running on port ${port}`);
});
runSockets(server);
