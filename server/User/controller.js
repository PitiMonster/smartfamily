const User = require("./model");
const catchAsync = require("./../utils/catchAsync");
const AppError = require("./../utils/appError");
const crudHandlers = require("./../controllers/handlers");

const FIELDS_TO_UPDATE = ["name", "surname", "profilePhoto"];

exports.getMe = (req, res, next) => {
  req.params.id = req.user.id;
  next();
};

exports.filterFieldsToUpdate = (req, res, next) => {
  const body = {};
  for (field of FIELDS_TO_UPDATE) {
    req.body[field] ? (body[field] = req.body[field]) : null;
  }
  req.body = body;
  next();
};

exports.getUser = crudHandlers.getOne(User);
exports.getAllUsers = crudHandlers.getAll(User);
exports.updateUser = crudHandlers.updateOne(User);

exports.handleFCMToken = catchAsync(async (req, res, next) => {
  const { action } = req.params;
  console.log(action);
  const { token } = req.body;
  if (token) {
    if (action === "add") {
      await User.findByIdAndUpdate(req.user._id, {
        $addToSet: { fcmTokens: token },
      });
    } else if (action === "delete") {
      await User.findByIdAndUpdate(req.user._id, {
        $pull: { fcmTokens: token },
      });
    }
  }
  return res.status(200).json({});
});
