const expect = require("chai").expect;
const sinon = require("sinon");
const mongoose = require("mongoose");

const dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });

const ShoppingItem = require("../ShoppingItem/model");
const Family = require("../Family/model");
const shoppingItemController = require("../ShoppingItem/controller");

const testError = require("../utils/testError");

describe("ShoppingItem Controller ", () => {
  let family;

  before((done) => {
    const DB = process.env.TEST_DATABASE.replace(
      "<PASSWORD>",
      process.env.DATABASE_PASSWORD
    );
    mongoose
      .connect(DB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      })
      .then(() => {
        console.log("BD CONNECTED SUCCESSFUL");
        return ShoppingItem.create({
          name: "Test shoppingItem",
          uniqueName: "5c0f66b979af55031b34728aTest shoppingItem",
          authorName: "TestTest",
          count: 12,
          _id: "5c0f66b979af55031b34728c",
        });
      })
      .then(() => {
        return Family.create({
          name: "Test family",
          shoppingList: ["5c0f66b979af55031b34728c"],
          chat: "5c0f66b979af55031b34728b",
          _id: "5c0f66b979af55031b34728a",
        });
      })
      .then((familyObject) => {
        family = familyObject;
        done();
      })
      .catch((err) => console.log(err));
  });

  it("Test if getShoppingItems returns list of family shopping items", (done) => {
    const req = {
      params: {
        familyId: "5c0f66b979af55031b34728a",
      },
    };
    const res = {
      status: (val) => {
        return {
          json: (object) => {
            return { ...object, statusCode: val };
          },
        };
      },
    };
    shoppingItemController
      .getShoppingItems(req, res, () => {})
      .then((response) => {
        expect(response).to.has.property("statusCode");
        expect(response).to.has.property("status");
        expect(response).to.has.property("data");
        expect(response.statusCode).to.be.equal(200);
        expect(response.status).to.be.equal("success");
        expect(response.data).to.be.an("array");
        expect(response.data[0]._id.toString()).to.be.equal(
          "5c0f66b979af55031b34728c"
        );

        done();
      })
      .catch((err) => console.log(err));
  });

  it("Test if createShoppingItem add newShoppingItem to family shoppingList", (done) => {
    const req = {
      params: {
        familyId: "5c0f66b979af55031b34728a",
      },
      user: {
        name: "Test",
        surname: "Test",
      },
      body: {
        name: "Test shoppingItem create",
        count: 12,
        description: null,
      },
      family: {
        _id: "5c0f66b979af55031b34728a",
        shoppingList: [],
        save: () => {},
      },
    };
    const res = {
      status: (val) => {
        return {
          json: (object) => {
            return { ...object, statusCode: val };
          },
        };
      },
    };
    shoppingItemController
      .createShoppingItem(req, res, () => {})
      .then((response) => {
        expect(response).to.has.property("statusCode");
        expect(response).to.has.property("status");
        expect(response).to.has.property("data");
        expect(response.statusCode).to.be.equal(201);
        expect(response.status).to.be.equal("success");
        expect(req.family.shoppingList.length).to.be.equal(1);
        expect(response.data._id.toString()).to.be.equal(
          req.family.shoppingList[0]._id.toString()
        );

        done();
      })
      .catch((err) => console.log(err));
  });

  it("Test getOneShoppingItem", (done) => {
    const req = {
      params: {
        id: "5c0f66b979af55031b34728c",
      },
    };
    const res = {
      status: (val) => {
        return {
          json: (object) => {
            return { ...object, statusCode: val };
          },
        };
      },
    };
    shoppingItemController
      .getOneShoppingItem(req, res, () => {})
      .then((response) => {
        expect(response).to.has.property("statusCode");
        expect(response).to.has.property("status");
        expect(response).to.has.property("data");
        expect(response.statusCode).to.be.equal(200);
        expect(response.status).to.be.equal("success");
        expect(response.data._id.toString()).to.be.equal(
          "5c0f66b979af55031b34728c"
        );

        done();
      })
      .catch((err) => console.log(err));
  });

  describe("Test updateShoppingItem", () => {
    it("Error: item with provided it not found", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728a",
        },
        family,
      };
      testError(
        shoppingItemController.updateShoppingItem,
        req,
        404,
        "Item with provided id not found",
        done
      );
    });

    it("Error: incorrect updateType provided", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728c",
        },
        family,
        body: {
          checkType: "incorrect check type",
        },
      };

      testError(
        shoppingItemController.updateShoppingItem,
        req,
        400,
        "Incorrect updateType provided",
        done
      );
    });

    it("Should check shopping item", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728c",
        },
        family,
        body: {
          checkType: "check",
        },
      };

      const res = {
        status: (val) => {
          return {
            json: (object) => {
              return { ...object, statusCode: val };
            },
          };
        },
      };

      shoppingItemController
        .updateShoppingItem(req, res, () => {})
        .then((response) => {
          expect(response).to.has.property("statusCode");
          expect(response).to.has.property("status");
          expect(response).to.has.property("data");
          expect(response.statusCode).to.be.equal(200);
          expect(response.status).to.be.equal("success");
          expect(response.data._id.toString()).to.be.equal(
            "5c0f66b979af55031b34728c"
          );
          expect(response.data.checked).to.be.equal(true);
          done();
        });
    });
    it("Should uncheck shopping item", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728c",
        },
        family,
        body: {
          checkType: "uncheck",
        },
      };

      const res = {
        status: (val) => {
          return {
            json: (object) => {
              return { ...object, statusCode: val };
            },
          };
        },
      };

      shoppingItemController
        .updateShoppingItem(req, res, () => {})
        .then((response) => {
          expect(response).to.has.property("statusCode");
          expect(response).to.has.property("status");
          expect(response).to.has.property("data");
          expect(response.statusCode).to.be.equal(200);
          expect(response.status).to.be.equal("success");
          expect(response.data._id.toString()).to.be.equal(
            "5c0f66b979af55031b34728c"
          );
          expect(response.data.checked).to.be.equal(false);
          done();
        });
    });
  });

  describe("Test deleteShoppingItem", () => {
    it("Error: No item with provided id belongs to the shopping list", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728a",
        },
        family,
      };
      testError(
        shoppingItemController.deleteShoppingItem,
        req,
        404,
        "No item with provided id belongs to the shopping list",
        done
      );
    });
    it("Should delete shopping list item", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728c",
        },
        family,
      };

      const res = {
        status: (val) => {
          return {
            json: (object) => {
              return { ...object, statusCode: val };
            },
          };
        },
      };

      shoppingItemController
        .deleteShoppingItem(req, res, () => {})
        .then((response) => {
          expect(response).to.has.property("statusCode");
          expect(response).to.has.property("status");
          expect(response).to.has.property("data");
          expect(response.statusCode).to.be.equal(202);
          expect(response.status).to.be.equal("success");
          expect(response.data._id.toString()).to.be.equal(
            "5c0f66b979af55031b34728c"
          );
          return ShoppingItem.findById(req.params.id);
        })
        .then((item) => {
          expect(item).to.be.a("null");
          return ShoppingItem.create({
            name: "Test shoppingItem",
            uniqueName: "5c0f66b979af55031b34728aTest shoppingItem",
            authorName: "TestTest",
            count: 12,
            _id: "5c0f66b979af55031b34728c",
          });
        })
        .then(() => {
          done();
        })
        .catch((err) => console.log(err));
    });
  });

  describe("Test patchShoppingItem", () => {
    it("Error: No item with provided id belongs to the shopping list", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728a",
        },
        family,
      };
      testError(
        shoppingItemController.patchShoppingItem,
        req,
        404,
        "No item with provided id belongs to the shopping list",
        done
      );
    });

    it("Should update shopping item", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728c",
        },
        family,
        body: {
          name: "updatedName",
          count: 123,
          description: "updatedDescription",
        },
      };

      const res = {
        status: (val) => {
          return {
            json: (object) => {
              return { ...object, statusCode: val };
            },
          };
        },
      };

      shoppingItemController
        .patchShoppingItem(req, res, () => {})
        .then((response) => {
          expect(response).to.has.property("statusCode");
          expect(response).to.has.property("status");
          expect(response).to.has.property("data");
          expect(response.statusCode).to.be.equal(200);
          expect(response.status).to.be.equal("success");
          expect(response.data._id.toString()).to.be.equal(
            "5c0f66b979af55031b34728c"
          );
          expect(response.data.name).to.be.equal(req.body.name);
          expect(response.data.count).to.be.equal(req.body.count);
          expect(response.data.description).to.be.equal(req.body.description);
          return ShoppingItem.deleteOne({ _id: response.data._id });
        })
        .then((item) => {
          return ShoppingItem.create({
            name: "Test shoppingItem",
            uniqueName: "5c0f66b979af55031b34728aTest shoppingItem",
            authorName: "TestTest",
            count: 12,
            _id: "5c0f66b979af55031b34728c",
          });
        })
        .then(() => {
          done();
        })
        .catch((err) => console.log(err));
    });
  });

  after((done) => {
    Family.deleteMany()
      .then(() => ShoppingItem.deleteMany())
      .then(() => mongoose.disconnect())
      .then(() => done());
  });
});
