const expect = require("chai").expect;
const sinon = require("sinon");
const mongoose = require("mongoose");

const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });

const User = require("../User/model");
const Family = require("../Family/model");
const Chat = require("../Chat/model");
const permissionController = require("../controllers/permissionController");

const testError = require("../utils/testError");

describe("Permission Controller ", () => {
  before((done) => {
    const DB = process.env.TEST_DATABASE.replace(
      "<PASSWORD>",
      process.env.DATABASE_PASSWORD
    );
    mongoose
      .connect(DB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      })
      .then(() => {
        console.log("BD CONNECTED SUCCESSFUL");
        return User.create({
          email: "test@test.com",
          name: "testes",
          surname: "tester",
          username: "test1",
          birthDate: "06/03/1999",
          sex: "male",
          role: "parent",
          password: "tester1234",
          passwordConfirm: "tester1234",
          _id: "5c0f66b979af55031b34728a",
        });
      })
      .then(() => {
        return Family.create({
          name: "Test family",
          members: ["5c0f66b979af55031b34728a"],
          chat: "5c0f66b979af55031b34728b",
          _id: "5c0f66b979af55031b34728c",
        });
      })
      .then(() => {
        return Chat.create({
          name: "Test chat",
          members: ["5c0f66b979af55031b34728a"],
          _id: "5c0f66b979af55031b34728b",
        });
      })
      .then(() => {
        done();
      })
      .catch((err) => console.log(err));
  });

  it("Test if wrong family object id returns error", (done) => {
    const req = {
      params: {
        familyId: "5c0f66b979af55031b34728b",
      },
    };
    const next = (err) => err;

    permissionController
      .isFamilyMember(req, {}, next)
      .then((res) => {
        expect(res).to.be.an("error");
        expect(res.message).to.be.equal("Family with that ID does not exist");
        expect(res.statusCode).to.be.equal(404);
        done();
      })
      .catch((err) => console.log(err));
  });

  it("Test user is not family member error", (done) => {
    const req = {
      params: {
        familyId: "5c0f66b979af55031b34728c",
      },
      user: {
        id: "5c0f66b979af55031b34728d",
      },
    };

    const next = (err) => err;

    permissionController.isFamilyMember(req, {}, next).then((res) => {
      expect(res).to.be.an("error");
      expect(res.message).to.be.equal(
        "You do not have permission to access this data"
      );
      expect(res.statusCode).to.be.equal(403);
      done();
    });
  });

  it("Test if req.family exists if family id is correct and user belongs to that family", (done) => {
    const req = {
      params: {
        familyId: "5c0f66b979af55031b34728c",
      },
      user: {
        id: "5c0f66b979af55031b34728a",
      },
    };
    permissionController
      .isFamilyMember(req, {}, () => {})
      .then(() => {
        expect(req).to.has.property("family");
        expect(req.family._id.toString()).to.be.equal(
          "5c0f66b979af55031b34728c"
        );
        done();
      });
  });

  it("Test if next function resolves in proper way", (done) => {
    const req = {
      params: {
        familyId: "5c0f66b979af55031b34728c",
      },
      user: {
        id: "5c0f66b979af55031b34728a",
      },
    };
    const next = () => "correct";
    permissionController.isFamilyMember(req, {}, next).then((res) => {
      expect(res).to.be.equal("correct");
      done();
    });
  });

  describe("Test isChatMember", () => {
    it("Error: Chat with that ID does not exist", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728a",
        },
      };
      testError(
        permissionController.isChatMember,
        req,
        404,
        "Chat with that ID does not exist",
        done
      );
    });
    it("Error: You do not have permission to access this data", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728b",
        },
        user: {
          id: "5c0f66b979af55031b34728b",
        },
      };
      testError(
        permissionController.isChatMember,
        req,
        403,
        "You do not have permission to access this data",
        done
      );
    });

    it("Should work", (done) => {
      const req = {
        params: {
          id: "5c0f66b979af55031b34728b",
        },
        user: {
          id: "5c0f66b979af55031b34728a",
        },
        chat: {},
      };
      const next = (param = "no error") => param;

      permissionController.isChatMember(req, {}, next).then((response) => {
        expect(response).to.equal("no error");
        done();
      });
    });
  });

  describe("Test isRolePermitted", () => {
    it("Error: You are not permitted to this action", () => {
      const req = {
        user: {
          role: "parent",
        },
      };
      const next = (param = "no error") => param;

      const isRolePermitted = permissionController.isRolePermitted("child");
      const response = isRolePermitted(req, {}, next);
      expect(response).to.be.an("error");
      expect(response).to.has.property("statusCode");
      expect(response).to.has.property("message");
      expect(response.statusCode).to.equal(403);
      expect(response.message).to.equal("You are not permitted to this action");
    });
    it("Should be permitted", () => {
      const req = {
        user: {
          role: "parent",
        },
      };
      const next = (param = "no error") => param;

      const isRolePermitted = permissionController.isRolePermitted("parent");
      const response = isRolePermitted(req, {}, next);
      expect(response).to.equal("no error");
    });
  });

  after((done) => {
    User.deleteMany()
      .then(() => Family.deleteMany())
      .then(() => Chat.deleteMany())
      .then(() => mongoose.disconnect())
      .then(() => done());
  });
});
