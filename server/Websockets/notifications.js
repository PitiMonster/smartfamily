const axios = require("axios");

let ioObject;

exports.runSockets = (io, socket) => {
  ioObject = io;

  socket?.on("connect notifications", (data) => {
    console.log("connecting notifications: ", data.userId);
    socket.join(data.userId);
    socket.emit("notifications connected", { status: "success" });
  });

  socket?.on("getChildLocation", (data) => {
    ioObject?.to(data.childId.toString()).emit("getLocation", data);
  });

  socket?.on("sendLocationToParent", (data) => {
    console.log(data);
    const token = process.env.POSITIONSTACK_TOKEN;
    (async () => {
      const { latitude, longitude } = data;
      const resp = await axios.get(
        `http://api.positionstack.com/v1/reverse?access_key=${token}&query=${latitude},${longitude}&limit=1`
      );
      console.log(resp.data.data[0]);
      const { name, locality } = resp.data.data[0];
      ioObject?.to(data.userId.toString()).emit("returnChildLocation", {
        ...data,
        address: name,
        city: locality,
      });
    })();
  });
};

exports.emitNotification = (notificationObject) => {
  ioObject
    ?.to(notificationObject.receiver._id.toString())
    .emit("new notification", {
      type: "notification",
      notification: notificationObject,
    });
};
