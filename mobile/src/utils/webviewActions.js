import {runSocket} from './websockets';
import {addTask, startTask} from './foregroundServices';
let webview = null;
import {PermissionsAndroid} from 'react-native';

import {registerFCM, requestUserPermission} from '../utils/fcm';

const getLocationPermissions = async () => {
  const isBackgroundGranted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_BACKGROUND_LOCATION,
    {
      title: 'Smart Family App Location Permission',
      message: 'Smart Family App needs access to your location ',
      buttonNeutral: 'Ask Me Later',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK',
    },
  );
  const isFineGranted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    {
      title: 'Smart Family App Location Permission',
      message: 'Smart Family App needs access to your location ',
      buttonNeutral: 'Ask Me Later',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK',
    },
  );
  const isCoarseGranted = await PermissionsAndroid.request(
    PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
    {
      title: 'Smart Family App Location Permission',
      message: 'Smart Family App needs access to your location ',
      buttonNeutral: 'Ask Me Later',
      buttonNegative: 'Cancel',
      buttonPositive: 'OK',
    },
  );
  console.log(isBackgroundGranted, isFineGranted, isCoarseGranted);
};

export const handleWebViewMessage = msg => {
  const parsedMsg = JSON.parse(msg);
  switch (parsedMsg.type) {
    case 'run websockets':
      const {userId, userRole} = parsedMsg.data;
      if (userRole === 'parent') {
        runSocket(userId);
      } else if (userRole === 'child') {
        addTask(userId);
        startTask();
        getLocationPermissions();
      }
      requestUserPermission();
      const sendTokenToBackend = token =>
        sendMessageToWeb({type: 'saveFcmToken', data: {token}});
      registerFCM(sendTokenToBackend);
      break;

    default:
      break;
  }
};

export const setWebView = webviewRef => (webview = webviewRef);

export const sendMessageToWeb = msg => {
  if (!webview) {
    return;
  }
  webview?.postMessage(JSON.stringify(msg));
};
