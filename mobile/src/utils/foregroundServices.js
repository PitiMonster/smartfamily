import ReactNativeForegroundService from '@supersami/rn-foreground-service';
import {runSocket} from './websockets';

export const startTask = () =>
  ReactNativeForegroundService.start({
    id: 144,
    title: 'Smart Family',
    message: 'Working in background!',
  });

export const stopForegroundService = () => {
  ReactNativeForegroundService.stop();
};

export const addTask = userId => {
  ReactNativeForegroundService.add_task(
    () => {
      runSocket(userId);
    },
    {
      delay: 1000000000000000000000000000,
      onLoop: true,
      taskId: 'taskid',
      onError: e => console.log(`Error logging:`, e),
    },
  );
};
