import messaging from '@react-native-firebase/messaging';
import {Alert} from 'react-native';
import {LocalNotification} from './notifications';

export const requestUserPermission = async () => {
  const authStatus = await messaging().requestPermission();
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  if (enabled) {
    console.log('Authorization status:', authStatus);
  }
};

export const registerFCM = sendTokenToBackend => {
  messaging()
    .getToken()
    .then(token => {
      sendTokenToBackend(token);
    });

  messaging().onTokenRefresh(token => {
    sendTokenToBackend(token);
  });

  // Register background handler
  messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
    LocalNotification(remoteMessage.data.text);
  });
  // reguster foregorund handler
  return messaging().onMessage(async remoteMessage => {
    Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
  });
};
