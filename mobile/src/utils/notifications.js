import PushNotification, {Importance} from 'react-native-push-notification';

export const createChannel = () => {
  PushNotification.createChannel(
    {
      channelId: '124', // (required)
      channelName: 'My channel', // (required)
      channelDescription: 'A channel to categorise your notifications', // (optional) default: undefined.
      playSound: false, // (optional) default: true
      soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
      importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
      vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
    },
    created => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
  );
};

export const configureForegroundNotifications = () => {
  PushNotification.configure({
    // (required) Called when a remote or local notification is opened or received
    onNotification: function (notification) {
      console.log('LOCAL NOTIFICATION ==>', notification);
    },

    popInitialNotification: true,
    requestPermissions: true,
  });
};

export const LocalNotification = title => {
  PushNotification.localNotification({
    channelId: '124',
    autoCancel: true,
    largeIcon: '',
    largeIconUrl:
      'https://res.cloudinary.com/dq7ionfvn/image/upload/v1638410737/SmartFamily/jxkhbgfsvpquihjylcrr.png',
    bigText: '',
    subText: '',
    title: title,
    message: '',
    vibrate: true,
    vibration: 300,
    playSound: true,
    soundName: 'default',
  });
};
