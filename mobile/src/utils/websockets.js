import {io} from 'socket.io-client';
import GetLocation from 'react-native-get-location';

export let socket;
export const runSocket = userId => {
  socket = io('https://immense-temple-96851.herokuapp.com', {
    reconnectionDelay: 1000,
    reconnection: true,
    transports: ['websocket'],
    agent: false,
    upgrade: false,
    rejectUnauthorized: false,
  });

  socket.on('connect', () => {
    // runNotificationSocketListeners();
    runGetLocationListener();
    runAppEmitters(userId);
  });
};

// export const runNotificationSocketListeners = () => {
//   socket.on('new notification', data => {
//     console.log(data);
//     const {type, notification} = data;
//     // send data by webview to web app
//   });
// };

export const runGetLocationListener = () => {
  socket.on('getLocation', data => {
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 10000,
    })
      .then(location => {
        const {longitude, latitude} = location;
        socket.emit('sendLocationToParent', {...data, longitude, latitude});
      })
      .catch(error => {
        const {code, message} = error;
        console.warn(code, message);
      });
  });
};

export const runAppEmitters = userId => {
  runEmitter('connect notifications', {userId});
};

export const runListener = listener => {
  listener(socket);
};

export const offListener = listener => {
  listener(socket);
};

export const runEmitter = (eventName, data) => {
  socket.emit(eventName, data);
};
