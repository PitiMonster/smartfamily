import React, {useEffect, useRef, useState} from 'react';
import {BackHandler} from 'react-native';

import {WebView} from 'react-native-webview';

import {
  handleWebViewMessage,
  sendMessageToWeb,
  setWebView,
} from '../utils/webviewActions';

import {createChannel} from '../utils/notifications';

const WebviewView = () => {
  const [isWebViewLoaded, setIsWebViewLoaded] = useState(false);

  const initWebview = `
  window.isRNWebView = true;
`;

  let webview = useRef();

  useEffect(() => {
    setWebView(webview);
  }, [isWebViewLoaded]);

  useEffect(() => {
    createChannel();
    const backAction = () => {
      sendMessageToWeb({type: 'goBack'});
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  }, []);

  return (
    <WebView
      source={{uri: 'https://smartfamily.vercel.app/auth'}}
      injectedJavaScriptBeforeContentLoaded={initWebview}
      ref={ref => (webview = ref)}
      onLoad={() => setIsWebViewLoaded(true)}
      onMessage={event => {
        handleWebViewMessage(event.nativeEvent.data);
      }}
    />
  );
};

export default WebviewView;
