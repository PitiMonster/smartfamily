import React from 'react';
import {SafeAreaView, StyleSheet, StatusBar} from 'react-native';

import WebviewView from './src/views/Webview';

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle={'light-content'} />
      <WebviewView />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
